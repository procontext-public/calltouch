<?php

namespace Procontext\CallTouch;

use Procontext\CallTouch\Exception\CallTouchDisabledException;
use Procontext\CallTouch\Exception\CallTouchEmptyTokenException;
use Procontext\CallTouch\Exception\CallTouchResponseException;

class CallTouch
{
    const REQUEST_URL = 'http://api.calltouch.ru/widget-service/v1/api/widget-request/user-form/create';

    const RESPONSE_SUCCESS = 'success';
    const RESPONSE_WARNING = 'warning';
    const RESPONSE_FAILED = 'failed';

    private $enable;
	private $token;

    public function __construct()
    {
        $this->enable = env('CT_ENABLE', false);
        $this->token = env('CT_TOKEN', null);
    }

    public function send(CallTouchParams $params): array
    {
        if (!$this->isEnable()) {
            throw new CallTouchDisabledException();
        }

        if (!$this->token) {
            throw new CallTouchEmptyTokenException();
        }

        $curl = curl_init(self::REQUEST_URL);
        curl_setopt_array($curl, [
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode($params->export(), JSON_UNESCAPED_UNICODE),
            CURLOPT_HTTPHEADER => ["Access-Token: " . $this->token],
            CURLOPT_CONNECTTIMEOUT => 30
        ]);

        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($response === false) {
            $result = [
                'status' => self::RESPONSE_FAILED,
                'reason' => curl_error($curl)
            ];
        } elseif ($httpCode != 200) {
            $response = json_decode($response, true);
            $result = [
                'status' => (
                    (
                        isset($response['data']['apiErrorData']['errorCode']) 
                        && $response['data']['apiErrorData']['errorCode'] == 10007
                    )
                    || isset($response['data']['validationErrorData'])
                ) 
                    ? self::RESPONSE_WARNING
                    : self::RESPONSE_FAILED,
                'reason' => [
                    'http_code' => $httpCode,
                    'message' => $response['data']['apiErrorData']['errorMessage'] ??
                        $response['data']['validationErrorData']['violations'][0]['message'] ??
                        '',
                    'description' => $response['data']['apiErrorData']['errorDescription'] ?? ''
                ]
            ];
        } else {
            $response = json_decode($response, true);
            if (!isset($response['data']) || empty($response['data'])) {
                $result = [
                    'status' => self::RESPONSE_FAILED,
                    'reason' => $response['data']['apiErrorData']['errorMessage'] ?? '',
                    'api_request' => [
                        'url' => self::REQUEST_URL,
                        'params' => $params
                    ]
                ];
            } else {
                $result = [
                    'status' => self::RESPONSE_SUCCESS,
                    'id' => $response['data']['widgetRequestId'] ?? ''
                ];
            }
        }
        curl_close($curl);

        if ($result['status'] === self::RESPONSE_FAILED) {
            throw new CallTouchResponseException($result);
        }

        return $result;
    }

    public function isEnable(): bool
    {
        return $this->enable;
    }
}
