<?php

namespace Procontext\CallTouch\Exception;

use \Exception;
use Throwable;

class CallTouchEmptyTokenException extends Exception
{
    public function __construct($message = "Не указан CallTouch токен", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
