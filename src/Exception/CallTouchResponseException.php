<?php

namespace Procontext\CallTouch\Exception;

use Throwable;

class CallTouchResponseException extends CallTouchException
{
    protected $response;

    public function __construct($response = [], $message = 'Ошибка ответа CallTouch API', $code = 500, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
        $this->response = $response;
    }

    public function getResponse(): array
    {
        return $this->response;
    }
}
